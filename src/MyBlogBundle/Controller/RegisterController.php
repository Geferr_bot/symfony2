<?php

namespace MyBlogBundle\Controller;

use MyBlogBundle\Entity\Users;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

use Symfony\Component\Validator\Constraints\Email as EmailConstraint;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;

class RegisterController extends Controller
{
    /**
     * @Route("/")
     */
    public function indexAction(Request $request)
    {
        if(isset($_REQUEST['token'])){

            $user = $this->getDoctrine()
                ->getRepository('MyBlogBundle:Users')
                ->findOneByToken($_REQUEST['token']);

            if($user){

                $em = $this->getDoctrine()->getManager();
                $user->setToken('');
                $user->setStatus(1);
                $em->flush();

                $session = $request->getSession();
                $session->getFlashBag()->add('message', 'You confirmed and now finally you registered on site!');
                return $this->redirect($this->generateUrl('my_blog_homepage'));

            }else{
                $session = $request->getSession();
                $session->getFlashBag()->add('message', 'Token not found!');
            }


        }

        $users = new Users();

        //register form
        $form = $this->createFormBuilder($users)
            ->add('firstName', TextType::class, array('label' => 'First name'))
            ->add('lastName', TextType::class, array('label' => 'Last name'))
            ->add('email', TextType::class, array('label' => 'Email'))
            ->add('pass', TextType::class, array('label' => 'Password'))
            ->add('save', SubmitType::class, array('label' => 'Registration'))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if($this->checkEmail($users->getEmail())) {
                $users->setCreatedAt(new \DateTime());
                $users->setToken(rand(100000, 9999999));
                $users->setStatus(0);
                $em = $this->getDoctrine()->getManager();
                $em->persist($users);
                $em->flush();

                $data = array('name' => $users->getFirstName(), 'token' => $users->getToken(), 'email' => $users->getEmail());
                $templ = 'register.html.twig';

                $this->SendMail($data, $templ);

                $session = $request->getSession();
                $session->getFlashBag()->add('message', 'Your User register success, confirm registration on your email!');


            }else{
                $session = $request->getSession();
                $session->getFlashBag()->add('message', 'This email not correct or user found!');
            }

            return $this->redirect($this->generateUrl($request->get('_route'), $request->query->all()));
        }

        //form login
        $form2 = $this->createFormBuilder($users)
            ->add('email', TextType::class, array('label' => 'Email'))
            ->add('pass', TextType::class, array('label' => 'Password'))
            ->add('login', SubmitType::class, array('label' => 'Login'))
            ->getForm();

        $form2->handleRequest($request);

        if ($form2->isValid()) {

            $user = $this->getDoctrine()
                ->getRepository('MyBlogBundle:Users')
                ->findOneByEmail($users->getEmail());
            $session = $request->getSession();
            if($user && $user->getStatus() == 1){
                if($user->getPass() == $users->getPass()){
                    $session->getFlashBag()->add('message', 'user '.$users->getEmail().' login!');
                }else{
                    $session->getFlashBag()->add('message', 'password not validate!');
                }
            }else{
                $session->getFlashBag()->add('message', 'user '.$users->getEmail().' not found!');
            }
        }

        return $this->render('MyBlogBundle:Default:index.html.twig',array(
            'formReg' => $form->createView(), 'formLog' => $form2->createView(),
        ));
    }

    public function confirmAction(Request $request)
    {
        return $this->redirect('/');
    }
    public function SendMail($data, $templ){
        $message = \Swift_Message::newInstance()
            ->setSubject('Thank You for Register')
            ->setFrom('geferr@yandex.ru')
            ->setTo($data['email'])
            ->setBody(
                $this->renderView(
                // app/Resources/views/Emails/registration.html.twig
                    'MyBlogBundle:Emails:'.$templ,
                    $data
                ),
                'text/html'
            )
        ;
        $this->get('mailer')->send($message);
    }

    /**
     * @Route("/test")
     */
    public function SendMailsAction(){
        $this->SendMails();
    }

    protected function getBetweenDates(){
        $cur_date = new \DateTime();
        $cur_date = $cur_date->format('Y-m-d H:i:s');

        $yesterday = new \DateTime();
        $yesterday->modify('-1 day');
        $yesterday = $yesterday->format('Y-m-d H:i:s');

        $repository = $this->getDoctrine();
        $userRepo = $repository->getRepository('MyBlogBundle:Users');
        return $userRepo->getEmailBetweenDates($cur_date, $yesterday);
    }

    public function SendMails(){

        $repository = $this->getDoctrine();

        //return $this->getBetweenDates();
        die();

//        $message = \Swift_Message::newInstance()
//            ->setSubject('Thank You for Register')
//            ->setFrom('geferr@yandex.ru')
//            ->setTo('dsds@wqw.ee')
//            ->setBody(
//                $this->renderView(
//                // app/Resources/views/Emails/registration.html.twig
//                    'MyBlogBundle:Emails:sendstatistic.html.twig',
//                    $data
//                ),
//                'text/html'
//            )
//        ;
//        $this->get('mailer')->send($message);


        //return false;
    }

    protected function checkEmail($email){
        $emailConstraint = new EmailConstraint();

        $errors = $this->get('validator')->validateValue(
            $email,
            $emailConstraint
        );

        if(count($errors) > 0){
            return $errors;
            exit();
        }

        $user = $this->getDoctrine()
            ->getRepository('MyBlogBundle:Users')
            ->findOneByEmail($email);

        if($user){
            return false;
            exit();
        }

        return true;
    }
}

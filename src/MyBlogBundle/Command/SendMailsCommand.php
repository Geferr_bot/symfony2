<?php
// src/MyBlogBundle/Command/SendMailsCommand.php
namespace MyBlogBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;

use MyBlogBundle\Controller\RegisterController as Reg;

class SendMailsCommand extends ContainerAwareCommand
{
protected function configure()
{
    $this
        ->setName('app:send-mails')
        ->setDescription('Send mail for Alex.')
        ->setHelp("This command for send mail for Alex...")
    ;
}

protected function execute(InputInterface $input, OutputInterface $output)
{

    $cur_date = new \DateTime();
    $cur_date = $cur_date->format('Y-m-d H:i:s');

    $yesterday = new \DateTime();
    $yesterday->modify('-1 day');
    $yesterday = $yesterday->format('Y-m-d H:i:s');

    $repository = $this->getContainer()->get('doctrine');
    $userRepo = $repository->getRepository('MyBlogBundle:Users');

    $users = '';
    foreach ($userRepo->getEmailBetweenDates($cur_date, $yesterday, true) as $key => $user){
        $users .= '<tr>';
        $users .= '<td>'.$user->getFirstName().'</td>';
        $users .= '<td>'.$user->getLastName().'</td>';
        $users .= '<td>'.$user->getEmail().'</td>';
        $users .= '</tr>';
    }

    $mail_body = '<html><head></head><body><table>' .
        ' <tr><td>First name</td><td>Last name</td><td>email</td></tr>' .
        $users.
        ' </table></body></html>';
    $transport = $this->getContainer()->get('swiftmailer.transport');
//    $transport = Swift_MailTransport::newInstance();
    $mailer = \Swift_Mailer::newInstance($transport);
    $message = \Swift_Message::newInstance('-f %s')
            ->setSubject('Thank You for Register')
            ->setFrom('geferr@yandex.ru')
            ->setTo('geferr@yandex.ru')
            ->setBody($mail_body,
                'text/html'
            )
        ;

    $mailer->send($message);

    $output->writeln(print_r($mail_body));

    $output->writeln('Mails sent!');
}
}